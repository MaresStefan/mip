package controllers;

import java.util.Date;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import model.Appointment;
import util.DatabaseUtil;

public class AddAppointmentController {

	
	 @FXML
	  private Button appointmentSaveBtn;
	  private TextField entryAppId;
	  private TextField entryAppDate;
	  private TextField entryAppDiagnostic;
	  private TextField entryAppPrice;
	  private TextField entryAppRoom;
	  private TextField entryAppAnimalId;
	  private TextField entryAppMedicalId;
	  
	  @FXML
	  public void SaveAppointmentToDB() throws Exception
	  {
		  DatabaseUtil db=new DatabaseUtil();
		  db.setUp();
		  db.startTransaction();
		  
		  Appointment app=new Appointment();
		  app.setDate( new Date(entryAppDate.getText()));
		  app.setPrice(Float.parseFloat(entryAppPrice.getText()));
		  app.setIdAppointment(Integer.parseInt(entryAppAnimalId.getText()));
		  app.setDiagnostic(entryAppDiagnostic.getText());
		  
		  db.saveAppointment(app);
		  db.closeEntityManager();
		  
	  }
}
