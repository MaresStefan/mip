package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Animal;
import util.DatabaseUtil;

public class AddAnimalController {

	
	@FXML Button saveAnimalBtn;
	@FXML
	Button cancelAddAnimal;
	
	@FXML
    void cancelAddingAnimal() {
		
		Stage stage = (Stage) cancelAddAnimal.getScene().getWindow();
		stage.close();
    }
	  @FXML
	    private TextField entryAnimalId;

	    @FXML
	    private TextField entryAnimalName;

	    @FXML
	    private TextField entryAnimalAge;

	    @FXML
	    private TextField entryAnimalOwner;

	    @FXML
	    private TextField entryAnimalPhone;

	    @FXML
	    private TextField entryAnimalSpecies;

	  @FXML
	    void saveAnimalToDb() throws Exception {
		   Stage stage = (Stage) saveAnimalBtn.getScene().getWindow();
		  DatabaseUtil db=new DatabaseUtil();
		  db.setUp();
		  db.startTransaction();
		  
		  Animal a=new Animal();
		  a.setAge(Integer.parseInt(entryAnimalAge.getText()));
		  a.setIdAnimal(Integer.parseInt(entryAnimalId.getText()));
		  a.setName(entryAnimalName.getText());
		  a.setOwner(entryAnimalOwner.getText());
		  a.setOwnerPhone(entryAnimalPhone.getText());
		  a.setSpecies(entryAnimalSpecies.getText());
		  
		  db.saveAnimal(a);
		  db.commitTransaction();
		  db.closeEntityManager();
		  stage.close();
		  
	  }
}
