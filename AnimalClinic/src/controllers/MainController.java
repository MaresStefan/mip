package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableListBase;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Animal;
import model.Appointment;
import util.DatabaseUtil;

public class MainController implements Initializable{

	
	
	@FXML
	private ListView<String> listView;
	
	  @FXML
	    private Button addAnimal_btn;
	  
	  @FXML
	    private Button AddAppointment_Btn;
	  @FXML
	  private Button refreshBtn;
	  @FXML
	    void refreshList(ActionEvent event) {
		  listView.refresh();
		  System.out.println("list fresh");
	    }
	 
	  
	  
	  
	  @FXML
	   public void OpenAddAppointmentWindow() {
		  try {
				FXMLLoader fxmlLoader=new FXMLLoader(getClass().getResource("AddAppointmentView.fxml"));
				Parent root1 =(Parent) fxmlLoader.load();
				Stage stage=new Stage();
				stage.setTitle("Add a new appointment to database.");
				stage.setScene(new Scene(root1));
				stage.show();
			}
			catch(Exception e)
			{
				System.out.println("Can't load new window");
			}
	    }
	
	@FXML
	public void OpenAddAnimalWindow(ActionEvent event) {
		try {
			FXMLLoader fxmlLoader=new FXMLLoader(getClass().getResource("AddAnimalView.fxml"));
			Parent root1 =(Parent) fxmlLoader.load();
			Stage stage=new Stage();
			stage.setTitle("Add a new animal to database.");
			stage.setScene(new Scene(root1));
			stage.show();
		}
		catch(Exception e)
		{
			System.out.println("Can't load new window");
		}
    
	}
	
	public void populateListView() throws Exception
	{
		DatabaseUtil db=new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		List<Appointment> appointments=(List<Appointment>)db.appointmentsFromDB();
		ObservableList<String> animalNamesList=getAnimalname(appointments);
		listView.setItems(animalNamesList);
		listView.refresh();
		db.closeEntityManager();
	}
	
//	public void getInfofromListView()
//	{
//		listView.setOnMouseClicked(new EventHandler<MouseEvent>()
//		{
//		    @Override
//		    public void handle(MouseEvent click) {
//		        if (click.getClickCount() == 2) {
//		           String currentItemSelected = listView.getSelectionModel().getSelectedItem().toString();
//		           System.out.println(currentItemSelected);
//		           lblName.concat(currentItemSelected);
//		        }
//		    }
//		});
//		
//	}
	
	public ObservableList<String> getAnimalname(List<Appointment> appointments)
	{
		ObservableList<String>names=FXCollections.observableArrayList();
		for(Appointment a: appointments)
		{
			names.add(a.getIdAppointment()+" "+a.getAnimal().getName()+" belongs to "+a.getAnimal().getOwnerPhone()+" "+a.getDiagnostic()+" "+a.getPrice());
			
		}
		return names;
	}
	
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		try {
			populateListView();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}


