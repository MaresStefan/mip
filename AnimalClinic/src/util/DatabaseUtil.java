package util;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Appointment;
import model.MedicalStaff;

public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	public void setUp() throws Exception
	{
		entityManagerFactory = Persistence.createEntityManagerFactory("AnimalClinic");
		entityManager=entityManagerFactory.createEntityManager();
	}
	
	
	public void startTransaction()
	{
		entityManager.getTransaction().begin();
	}

	public void commitTransaction()
	{
		entityManager.getTransaction().commit();
	}
	
	public void closeEntityManager()
	{
		entityManager.close();
	}
	
	public void saveAnimal(Animal animal)
	{
		entityManager.persist(animal);
	}
	
	public void saveMedicalStaff(MedicalStaff medic)
	{
		entityManager.persist(medic);
	}
	
	public void saveAppointment(Appointment app)
	{
		entityManager.persist(app);
	}
	
	public void deleteAppointment(int id) {
		Appointment toFound=entityManager.find(Appointment.class, id);
		entityManager.remove(toFound);
	}
	
	public void deleteAnimal(int id) {
		Animal toFound=entityManager.find(Animal.class, id);
		entityManager.remove(toFound);
	}
	
	public void deleteMedicalStaff(int id) {
		MedicalStaff toFound=entityManager.find(MedicalStaff.class, id);
		entityManager.remove(toFound);
	}
	
	public void updateAppointmentPrice(int id, float price)
	{
		try
		{
			Appointment toFound=(Appointment)entityManager.find(Appointment.class, id);
			toFound.setPrice(price);
		}
		catch (Exception e) {
			System.out.println("There is no appointment with id: "+id);
		}
	}
	
	public void updateAppointmentDiagnostic(int id, String diagnostic)
	{
		try
		{
			Appointment toFound=(Appointment)entityManager.find(Appointment.class, id);
			toFound.setDiagnostic(diagnostic);;
		}
		catch (Exception e) {
			System.out.println("There is no appointment with id: "+id);
		}
	}
	
	public void updateAppointmentDate(int id, Date date)
	{
		try
		{
			Appointment toFound=(Appointment)entityManager.find(Appointment.class, id);
			toFound.setDate(date);;
		}
		catch (Exception e) {
			System.out.println("There is no appointment with id: "+id);
		}
	}
	
	public void updateAnimalName(int id, String name)
	{
		try
		{
			Animal toFound=(Animal)entityManager.find(Animal.class, id);
			toFound.setName(name);
		}
		catch(Exception e)
		{
			System.out.println("There is no animal with id: "+id);
		}
	}
	
	public void updateAnimalOwner(int id, String owner)
	{
		try
		{
			Animal toFound=(Animal)entityManager.find(Animal.class, id);
			toFound.setOwner(owner);;
		}
		catch(Exception e)
		{
			System.out.println("There is no animal with id: "+id);
		}
	}
	
	public void updateAnimalOwnerPhone(int id, String phone)
	{
		try
		{
			Animal toFound=(Animal)entityManager.find(Animal.class, id);
			toFound.setOwnerPhone(phone);;
		}
		catch(Exception e)
		{
			System.out.println("There is no animal with id: "+id);
		}
	}
	
	public void updateAnimalAge(int id, int age)
	{
		try
		{
			Animal toFound=(Animal)entityManager.find(Animal.class, id);
			toFound.setAge(age);;
		}
		catch(Exception e)
		{
			System.out.println("There is no animal with id: "+id);
		}
	}
	
	
	public void updateMedicalStaffName(int id, String name)
	{
		try
		{
			MedicalStaff toFound=(MedicalStaff)entityManager.find(MedicalStaff.class, id);
			toFound.setName(name);
		}
		catch(Exception e)
		{
			System.out.println("there is no medic with id: "+id);
		}
	}
	
	public void updateMedicalStaffSpecialization(int id, String specialization)
	{
		try
		{
			MedicalStaff toFound=(MedicalStaff)entityManager.find(MedicalStaff.class, id);
			toFound.setSpecialization(specialization);
		}
		catch(Exception e)
		{
			System.out.println("there is no medic with id: "+id);
		}
	}
	
	public void removeAnimal(int id)
	{
		try
		{
			Animal toFound=(Animal)entityManager.find(Animal.class, id);
			entityManager.remove(toFound);
		}
		catch(Exception e)
		{
			System.out.println("There is no animal with id: "+id);
		}
	}
	
	public void removeAppointment(int id)
	{
		try
		{
			Appointment toFound=(Appointment)entityManager.find(Appointment.class, id);
			entityManager.remove(toFound);
		}
		catch(Exception e)
		{
			System.out.println("There is no appointment with id: "+id);
		}
	}
	
	public void removeMedicalStaff(int id)
	{
		try
		{
			MedicalStaff toFound=(MedicalStaff)entityManager.find(MedicalStaff.class, id);
			entityManager.remove(toFound);
		}
		catch(Exception e)
		{
			System.out.println("There is no medic with id: "+id);
		}
	}
	
	
	public void printAllAnimalsFromDB()
	{
		List<Animal> results = entityManager.createNativeQuery("Select * from animalclinic.animal", Animal.class).getResultList();
		for (Animal animal: results)
		{
			System.out.println("Animal: "+animal.getName()+" has id: "+ animal.getIdAnimal()+" belongs to:  "+animal.getOwner());
		}
	}
	
	public void printAllAppointmentsFromDB()
	{
		List<Appointment> results = entityManager.createNativeQuery("Select * from animalclinic.appointment", Appointment.class).getResultList();
		for (Appointment app: results)
		{
			System.out.println("Animal: "+app.getAnimal().getName()+" needs: "+ app.getDiagnostic()+" cost:  "+app.getPrice());
		}
	}
	
	public void printMedicalStaffFromDB()
	{
		List<MedicalStaff> results = entityManager.createNativeQuery("Select * from animalclinic.medical_staff", MedicalStaff.class).getResultList();
		for (MedicalStaff medic: results)
		{
			System.out.println("Medic: "+medic.getName()+" Specialization: "+ medic.getSpecialization()+" experience: "+medic.getExpYears()+" years");
		}
	}
	
	public void printSortedAppointments()
	{
		String query="SELECT * from animalclinic.appointment order by date";
		List<Appointment> appointments=entityManager.createNativeQuery(query, Appointment.class).getResultList();
		//appointments.sort(Comparator.comparing(Appointment::getDate));
		for(Appointment app:appointments)
		{
			System.out.println("Appointment: "+app.getIdAppointment()+" "+app.getAnimal().getName()+" "+app.getDate().toString()+" "+ app.getDiagnostic()+" "+app.getPrice());
		}
	}
	
	public List<Appointment> appointmentsFromDB ()
	{
		List<Appointment> appointmentList=(List<Appointment>)entityManager.createNativeQuery("Select * from animalclinic.appointment", Appointment.class).getResultList();
		return appointmentList;
	}
}
