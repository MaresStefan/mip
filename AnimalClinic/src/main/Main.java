package main;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application
{

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			BorderPane root=(BorderPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
			Scene scene =new Scene(root, 800,800);
			primaryStage.setScene(scene);
			primaryStage.show();
			
		} catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		
	}
	
	public static void main(String[] args)
	{
		launch(args);
	}
	
}