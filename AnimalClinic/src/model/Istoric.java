package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the istoric database table.
 * 
 */
@Entity
@NamedQuery(name="Istoric.findAll", query="SELECT i FROM Istoric i")
public class Istoric implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_istoric")
	private int idIstoric;

	@Temporal(TemporalType.DATE)
	private Date data;

	private String diagnostic;

	@Column(name="id_animal_fk")
	private int idAnimalFk;

	private float pret;

	public Istoric() {
	}

	public int getIdIstoric() {
		return this.idIstoric;
	}

	public void setIdIstoric(int idIstoric) {
		this.idIstoric = idIstoric;
	}

	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getDiagnostic() {
		return this.diagnostic;
	}

	public void setDiagnostic(String diagnostic) {
		this.diagnostic = diagnostic;
	}

	public int getIdAnimalFk() {
		return this.idAnimalFk;
	}

	public void setIdAnimalFk(int idAnimalFk) {
		this.idAnimalFk = idAnimalFk;
	}

	public float getPret() {
		return this.pret;
	}

	public void setPret(float pret) {
		this.pret = pret;
	}

}