package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the medical_staff database table.
 * 
 */
@Entity
@Table(name="medical_staff")
@NamedQuery(name="MedicalStaff.findAll", query="SELECT m FROM MedicalStaff m")
public class MedicalStaff implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="idmedical_staff")
	private int idmedicalStaff;

	private int age;

	@Column(name="exp_years")
	private int expYears;

	private String name;

	private String specialization;

	//bi-directional many-to-one association to Appointment
	@OneToMany(mappedBy="medicalStaff")
	private List<Appointment> appointments;

	public MedicalStaff() {
	}

	public int getIdmedicalStaff() {
		return this.idmedicalStaff;
	}

	public void setIdmedicalStaff(int idmedicalStaff) {
		this.idmedicalStaff = idmedicalStaff;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getExpYears() {
		return this.expYears;
	}

	public void setExpYears(int expYears) {
		this.expYears = expYears;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpecialization() {
		return this.specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public List<Appointment> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	public Appointment addAppointment(Appointment appointment) {
		getAppointments().add(appointment);
		appointment.setMedicalStaff(this);

		return appointment;
	}

	public Appointment removeAppointment(Appointment appointment) {
		getAppointments().remove(appointment);
		appointment.setMedicalStaff(null);

		return appointment;
	}

}