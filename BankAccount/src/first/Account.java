package first;

public class Account {
	private String accountNumber;
	private float balance;
	private String customerName;
	private String email;
	private String phoneNumber;
	
	/**
	 * This function add funds to the current balance
	 * @param amount The amount that needs to be added
	 */
	public void addFunds(float amount) {
		this.balance+=amount;
		System.out.println();
	}
	
	/**
	 * This function exctract funds from the current balance.
	 * In if balance is lower than ammount the function print a warning message to the console. 
	 * @param amount The amount to withdraw
	 */
	public void withdraw(float amount) {
		if(this.balance<amount) {
			System.out.println("Cannot perform withdraw! Not enough money in the account.");
		}
		else {
			balance-=amount;
			System.out.println("Withdraw finished successfully.");
		}
	}
	
	public Account(String accountNumber, String customerName, String email, String phoneNumber) {
		super();
		this.accountNumber = accountNumber;
		this.customerName = customerName;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public float getBalance() {
		return balance;
	}
	public void setBalance(float balance) {
		this.balance = balance;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
}
