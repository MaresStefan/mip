package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the personal_medical database table.
 * 
 */
@Entity
@Table(name="personal_medical")
@NamedQuery(name="PersonalMedical.findAll", query="SELECT p FROM PersonalMedical p")
public class PersonalMedical implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="idpersonal_medical")
	private int idpersonalMedical;

	private String name;
	private String e_mail;
	private String telefon;

	public String getE_mail() {
		return e_mail;
	}
	public void setE_mail(String e_mail) {
		this.e_mail = e_mail;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}


	//bi-directional many-to-one association to Programari
	@OneToMany(mappedBy="personalMedical")
	private List<Programari> programaris;

	public PersonalMedical() {
	}

	public int getIdpersonalMedical() {
		return this.idpersonalMedical;
	}

	public void setIdpersonalMedical(int idpersonalMedical) {
		this.idpersonalMedical = idpersonalMedical;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Programari> getProgramaris() {
		return this.programaris;
	}

	public void setProgramaris(List<Programari> programaris) {
		this.programaris = programaris;
	}

	public Programari addProgramari(Programari programari) {
		getProgramaris().add(programari);
		programari.setPersonalMedical(this);

		return programari;
	}

	public Programari removeProgramari(Programari programari) {
		getProgramaris().remove(programari);
		programari.setPersonalMedical(null);

		return programari;
	}

}