package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the programari database table.
 * 
 */
@Entity
@NamedQuery(name="Programari.findAll", query="SELECT p FROM Programari p")
public class Programari implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idprogramari;

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	private int idAnimal;

	//bi-directional many-to-one association to PersonalMedical
	@ManyToOne
	@JoinColumn(name="idpersonal_medical")
	private PersonalMedical personalMedical;

	public Programari() {
	}

	public int getIdprogramari() {
		return this.idprogramari;
	}

	public void setIdprogramari(int idprogramari) {
		this.idprogramari = idprogramari;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getIdAnimal() {
		return this.idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public PersonalMedical getPersonalMedical() {
		return this.personalMedical;
	}

	public void setPersonalMedical(PersonalMedical personalMedical) {
		this.personalMedical = personalMedical;
	}

}