package util;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.PersonalMedical;
import model.Programari;

public class DatabaseUtil {

	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	
	
	/**
	 * This function initialize the entity manager
	 * @throws Exception
	 */
	public void setUP() throws Exception {
		entityManagerFactory=Persistence.createEntityManagerFactory("PetShopJPA");
		entityManager=entityManagerFactory.createEntityManager();
	}
	
	
	
	/**
	 * This function receive an animal object as parameter and save the animal to database
	 * @param animal
	 */
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal); 
	}
	

	/**
	 * This function receive a medical staff object as parameter and save it to database
	 * @param personalMedical
	 */
	public void savePersonalMedical(PersonalMedical personalMedical) {
		entityManager.persist(personalMedical); 
	}

	
	
	/**
	 * This function receive an appointment object as parameter and save it to database
	 * @param programare
	 */
	public void saveAppointment(Programari programare) {
		entityManager.persist(programare); 
	}
	
	
	
	/**
	 * This function start the connection to database
	 */
	public void startTranzaction() {
		entityManager.getTransaction().begin();
	}
	
	
	/**
	 * This function save changes to database
	 */
	public void commitTranzaction() {
		entityManager.getTransaction().commit();
	}
	
	
	/**
	 * This function close the connection to database
	 */
	public void closeEntityManager() {
		entityManager.close();
	}
	
	

	/**
	 * This function print to console all animals from database
	 */
	public void printAllAnimalsFromDB() {
		List<Animal> results=entityManager.createNativeQuery("Select * from petshop.animal", Animal.class).getResultList();
		for(Animal animal:results)
		{
			System.out.println("Animal: "+animal.getName()+" has Id: "+animal.getIdAnimal());
		}
	}
	

	/**
	 * This function print to console all appintments from database
	 */
	public void printAllAppointmentsFromDB() {
		List<Programari> results=entityManager.createNativeQuery("Select * from petshop.programari", Programari.class).getResultList();
		for(Programari programare:results)
		{
			System.out.println("Programare: "+programare.getDate()+" has Id: "+programare.getIdprogramari());
		}
	}
	

	/**
	 * This function print to console all medical staff from database
	 */
	public void printAllMedicalStuffFromDB() {
		List<PersonalMedical> results=entityManager.createNativeQuery("Select * from petshop.personal_medical", PersonalMedical.class).getResultList();
		for(PersonalMedical personal:results)
		{
			System.out.println("Personal Medical: "+personal.getName()+" has Id: "+personal.getIdpersonalMedical()+" telefon: "+personal.getTelefon());
		}
	}
	

	/**
	 * This function delete an animal from database based on it's id received as a parameter
	 * @param id
	 */
	public void deleteAnimalFromDB(int id) {
		Animal toFound=entityManager.find(Animal.class, id);
		entityManager.remove(toFound);
	}
	

	/**
	 *  This function delete a medical stuff from database based on it's id received as a parameter
	 * @param id
	 */
	public void deleteMedicalStuffFromDB(int id) {
		PersonalMedical toFound=entityManager.find(PersonalMedical.class, id);
		entityManager.remove(toFound);
	}
	

	/**
	 *  This function delete an appointment from database based on it's id received as a parameter
	 * @param id
	 */
	public void deleteAppointmentFromDB(int id) {
		Programari toFound=entityManager.find(Programari.class, id);
		entityManager.remove(toFound);
	}
	

	/**
	 * This function update an animal with name, weight, species, ownerName and age 
	 * @param id
	 * @param newName
	 * @param weight
	 * @param species
	 * @param ownerName
	 * @param age
	 */
	public void updateAnimalfromDB(int id, String newName, float weight, String species, String ownerName, int age) {
		Animal toFound=entityManager.find(Animal.class, id);
		toFound.setName(newName);
		toFound.setAge(age);
		toFound.setOwnerName(ownerName);
		toFound.setSpecies(species);
		toFound.setWeight(weight);
	}
	

	/**
	 * This function update an appointment with date
	 * @param id
	 * @param date
	 */
	public void updateAppointmentfromDB(int id, Date date) {
		Programari toFound=entityManager.find(Programari.class, id);
		toFound.setDate(date);
	}
	

	/**This function update a medical staff with name, phone and e_mail
	 * @param id
	 * @param newName
	 * @param telefon
	 * @param e_mail
	 */
	public void updateMedicalStufffromDB(int id, String newName, String telefon, String e_mail) {
		PersonalMedical toFound=entityManager.find(PersonalMedical.class, id);
		toFound.setName(newName);
		toFound.setE_mail(e_mail);
		toFound.setTelefon(telefon);
	}
	
	public void listaProgramariSortateDupaData() {
		System.out.println("Lista programarilor sortate dupa Data:");
		List<Programari> results=entityManager.createNativeQuery("Select * from pethshop.programari",Programari.class).getResultList();
		results.sort(Comparator.comparing(Programari::getDate));
		for (Programari programare : results) {
			System.out.println("Programarea cu ID - ul "+programare.getIdprogramari()+" executata de Personalul Medical "+programare.getPersonalMedical().getName());
		}
	}

	}
