package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the booking_type database table.
 * 
 */
@Entity
@Table(name="booking_type")
@NamedQuery(name="BookingType.findAll", query="SELECT b FROM BookingType b")
public class BookingType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int bookingType_id;

	private byte atCinema;

	private byte byPhone;

	private byte online;

	//bi-directional many-to-one association to Booking
	@OneToMany(mappedBy="bookingType")
	private List<Booking> bookings;

	public BookingType() {
	}

	public int getBookingType_id() {
		return this.bookingType_id;
	}

	public void setBookingType_id(int bookingType_id) {
		this.bookingType_id = bookingType_id;
	}

	public byte getAtCinema() {
		return this.atCinema;
	}

	public void setAtCinema(byte atCinema) {
		this.atCinema = atCinema;
	}

	public byte getByPhone() {
		return this.byPhone;
	}

	public void setByPhone(byte byPhone) {
		this.byPhone = byPhone;
	}

	public byte getOnline() {
		return this.online;
	}

	public void setOnline(byte online) {
		this.online = online;
	}

	public List<Booking> getBookings() {
		return this.bookings;
	}

	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

	public Booking addBooking(Booking booking) {
		getBookings().add(booking);
		booking.setBookingType(this);

		return booking;
	}

	public Booking removeBooking(Booking booking) {
		getBookings().remove(booking);
		booking.setBookingType(null);

		return booking;
	}

}