/**
 * 
 */
package util;

import java.sql.Date;
import java.sql.Time;
import java.util.Comparator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Booking;
import model.Customer;
import model.Movie;
import model.Schedule;
/**
 * @author Stefan
 *
 */
public class DatabaseUtil {
	
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	/**
	 * This function initialize entityManagerFactory and entityManager
 	 * @throws Exception if cannot connect to database
	 */
	public void setUp() throws Exception {
		entityManagerFactory=Persistence.createEntityManagerFactory("CinemaJPA1");
		entityManager = entityManagerFactory.createEntityManager();
	}
	
	
	/**
	 * This function start the connection to the database.
	 */
	public void startTranzaction() {
		entityManager.getTransaction().begin();
	}
	
	/**
	 * This function save changes to database.

	 */
	public void commitTranzaction() {
		entityManager.getTransaction().commit();
	}
	
	/**
	 * This function close the connection to database.
	 */
	public void closeEntityManager() {
		entityManager.close();
	}
	
	/**
	 * This function save a booking to database.
	 * @param booking The booking that needs to be saved.
	 */
	public void saveBooking(Booking booking) {
		entityManager.persist(booking);
	}
	
	/**
	 * This function save a schedule to database
	 * @param schedule The schedule that needs to be saved.
	 */
	public void saveSchedule(Schedule schedule) {
		entityManager.persist(schedule);
	}
	
	/**
	 * This function save a movie to database
	 * @param movie The movie that needs to be saved.
	 */
	public void saveMovie(Movie movie) {
		entityManager.persist(movie);
	}
	
	/**
	 * This function save a customer to database 
	 * @param customer The customer that needs to be saved.
	 */
	public void saveCustomer(Customer customer) {
		entityManager.persist(customer);
	}
	
	/**
	 * This function delete a booking from database.
	 * @param id The id of the booking.
	 */
	public void deleteBookingFromDB(int id) {
		Booking booking=entityManager.find(Booking.class, id);
		entityManager.remove(booking);
	}
	
	/**
	 * This function delete a customer from database.
	 * @param id The id of the booking.
	 */
	public void deleteCustomerFromDB(int id) {
		Customer customer=entityManager.find(Customer.class, id);
		entityManager.remove(customer);
	}
	
	/**
	 * This function delete a movie from database.
	 * @param id The id of the movie
	 */
	public void deleteMovieFromDB(int id) {
		Movie movie=entityManager.find(Movie.class, id);
		entityManager.remove(movie);
	}
	
	/**
	 * This function delete a schedule from database.
	 * @param id The id of the movie
	 */
	public void deleteScheduleFromDB(int id) {
		Schedule schedule=entityManager.find(Schedule.class, id);
		entityManager.remove(schedule);
	}
	
	/**
	 * This function update a schedule
	 * @param id The id of the schedule that needs to be updated
	 * @param newDate The new date of the movie
	 * @param newTime The new Time of the movie
	 * @param newHall The new hall of the movie
	 */
	public void updateScheduleFromDB(int id, Date newDate, Time newTime, int newHall) {
		Schedule schedule=entityManager.find(Schedule.class, id);
		schedule.setDate(newDate);
		schedule.setHall(newHall);
		schedule.setTime(newTime);
	}
	
	/**
	 * This function update a movie
	 * @param id  id of the movie that needs to be updated
	 * @param newGenre The new genre of the movie
	 * @param newLength The new length of the movie
	 * @param newQuality The new quality of the movie
	 * @param newTitle The new title of the movie
	 * @param newRating The new rating of the movie
	 */
	public void updateMovieFromDB(int id, String newGenre, int newLength, String newQuality, String newTitle, float newRating) {
		Movie movie=entityManager.find(Movie.class, id);
		movie.setGenre(newGenre);
		movie.setLength(newLength);
		movie.setQuality(newQuality);
		movie.setRating(newRating);
		movie.setTitle(newTitle);
	}
	
	/**
	 * This function update a customer
	 * @param id The id of the customer that needs to be updated
	 * @param newFirstName The new first name of the customer
	 * @param newLastName The new last name of the customer
	 * @param newEmail The new e_mail of the customer
	 */
	public void updateCustomerFromDB(int id, String newFirstName, String newLastName, String newEmail) {
		Customer customer=entityManager.find(Customer.class, id);
		customer.setEMail(newEmail);
		customer.setFirstName(newFirstName);
		customer.setLastName(newLastName);
	}
	
	/**
	 * This function update a booking
	 * @param id The id of the booking that needs to be updated
	 * @param newRow The new row of the booking
	 * @param newSeat The new seat of the booking
	 */
	public void updateBooking(int id, int newRow, int newSeat) {
		Booking booking=entityManager.find(Booking.class, id);
		booking.setSeat(newSeat);
		booking.setRow(newRow);
	}
	
	/**
	 * This function print all movies form database to the console.
	 */
	public void printAllMoviesFromDB() {
		List<Movie> results = entityManager.createNativeQuery("SELECT * FROM cinema.movie", Movie.class).getResultList();
		for(Movie movie:results)
		{
			System.out.println(movie.getTitle()+", "+movie.getGenre()+", "+movie.getQuality());
		}
	}
	
	/**
	 * This function print all customers from database to the console.
	 */
	public void printAllCustomersFromDB() {
		List<Customer> results = entityManager.createNativeQuery("SELECT * FROM cinema.customer", Customer.class).getResultList();
		for(Customer customer:results)
		{
			System.out.println(customer.getFirstName()+", "+customer.getLastName()+", "+customer.getEMail());
		}
	}
	
	/**
	 * This function print all schedules from database to the console.
	 */
	public void printAllSchedulesFromDB() {
		List<Schedule> results = entityManager.createNativeQuery("SELECT * FROM cinema.schedule", Schedule.class).getResultList();
		for(Schedule schedule:results)
		{
			System.out.println(schedule.getMovie().getTitle()+", Hall: "+schedule.getHall()+", "+schedule.getDate()+", "+schedule.getTime());
		}
	}
	
	/**
	 * This function sort all schedules(movies) by time.
	 */
	public void printSortedMoviesByHour() {
		List<Schedule> results = entityManager.createNativeQuery("SELECT * FROM cinema.schedule", Schedule.class).getResultList();
		results.sort(Comparator.comparing(Schedule::getTime));
		for(Schedule schedule:results) {
			System.out.println(schedule.getMovie().getTitle()+", hall "+schedule.getHall()+" "+schedule.getTime());
		}
	}
	
	
	

}
